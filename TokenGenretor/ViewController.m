//
//  ViewController.m
//  TokenGenretor
//
//  Created by click labs 115 on 10/7/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    UITextField *txtTokenInput;
    BOOL isvalidemail ;
    NSMutableArray * tokenStore;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    txtTokenInput = [UITextField new];
    txtTokenInput.frame = CGRectMake(80, 100, 220, 30);
    txtTokenInput.layer.borderWidth = 1.0f;
    txtTokenInput.layer.borderColor = [UIColor blackColor].CGColor ;
    txtTokenInput.placeholder = @"Enter Valid Email : ";
    txtTokenInput.layer.cornerRadius = 6.0f;
    txtTokenInput.font = [UIFont fontWithName:@"Arial"size:20.0f];
    txtTokenInput.borderStyle = UITextBorderStyleBezel;
    txtTokenInput.clearButtonMode = UITextFieldViewModeAlways;
    txtTokenInput.returnKeyType = UIReturnKeyGo;
    txtTokenInput.keyboardType = UIKeyboardTypeEmailAddress;
    txtTokenInput.keyboardAppearance = UIKeyboardAppearanceDark;
    txtTokenInput.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    txtTokenInput.secureTextEntry =NO;
   txtTokenInput.autocapitalizationType = UITextAutocapitalizationTypeNone;
    txtTokenInput.delegate = self;
    [self.view addSubview:txtTokenInput];
    
    tokenStore = [ NSMutableArray new];
    
    // Do any additional setup after loading the view, typically from a nib.
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesBegan:withEvent:");
    [self.view endEditing:YES];
    //[super touchesBegan:touches withEvent:event];
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    txtTokenInput.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f];
    txtTokenInput.layer.borderColor = [UIColor greenColor].CGColor;
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldDidBeginEditing");
    //textField.backgroundColor = [UIColor clearColor];
    txtTokenInput.layer.borderColor = [UIColor greenColor].CGColor;
   
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldEndEditing");
    textField.backgroundColor = [UIColor whiteColor];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"textFieldDidEndEditing");
    txtTokenInput.layer.borderColor = [UIColor redColor].CGColor;

}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    //Valid email address
    if([string isEqualToString:@" "])
    {
        if ([emailTest evaluateWithObject: txtTokenInput.text ] == YES)
        {
            [tokenStore addObject:txtTokenInput.text];
            txtTokenInput.text = @"";
            int xPose = 5;
            int yPose = 170;
            for (int i=0; i<[tokenStore count]; i++) {
                UIButton *btnToken = [UIButton new];
                btnToken.frame = CGRectMake(xPose, yPose, 180, 30);
                [btnToken setTitle:[tokenStore objectAtIndex:i] forState:UIControlStateNormal];
                [btnToken setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                btnToken.backgroundColor = [UIColor cyanColor];
                [self.view addSubview:btnToken];
                xPose = xPose+185;
                if (xPose%3 == 0) {
                    
                    xPose = 5 ;
                    yPose = yPose+35;
                }
            }
            return NO;
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Enter a valid email address." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            
            return NO;
        }
    }
    else
    {
        return YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
